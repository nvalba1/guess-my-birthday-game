from random import randint

name = input("Hi What is your name?: ")

for i in range(5):
    random_month = randint(1, 12)
    random_year = randint(1924, 2004)
    is_birthday_correct = input(f"Guess {i + 1}: {name}, were you born in {random_month}/{random_year}?")
    if is_birthday_correct in ("y".lower(), "yes".lower()):
        print("I knew it!")
        break
    elif is_birthday_correct in ("n".lower(), "no".lower()):
        if i < 4:
            print("Drat!  Lemme try again!")
        else:
            print("I have other things to do. Good bye.")
